from django import forms
from www.todolist import models


class TodoEntry_form(forms.ModelForm):
    class Meta:
        model = models.TodoEntry
        fields = ['title', 'description', 'priority', 'due_date', 'due_time']