# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_auto_20150421_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoentry',
            name='priority',
            field=models.CharField(default=b'N', max_length=10, choices=[(b'NONE', b'NONE'), (b'LOW', b'LOW'), (b'MEDIUM', b'MEDIUM'), (b'HIGH', b'HIGH')]),
        ),
    ]
