from django.db import models
from django.contrib.auth.models import User
from datetime import date


# Create your models here.
# User class
class TodoEntry(models.Model):
    # entry_id = pk user_id = fk 
    entry_id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500, blank=True)
    # A priority setting with 4 choices
    PRIORITY_CHOICES = (
        ('NONE', 'NONE'),
        ('LOW', 'LOW'),
        ('MEDIUM', 'MEDIUM'),
        ('HIGH', 'HIGH'),
    )
    #Date and time (require date and time format)
    priority = models.CharField(max_length=10, choices=PRIORITY_CHOICES, default='N')
    due_date = models.DateField('Due date')
    due_time = models.TimeField('Due time')
    creation_datetime = models.DateTimeField ('Creation date and time', default=date.today)
    #2 possible Status states (OPEN or DONE)
    STATUS_CHOICES = (
        ('OPEN', 'O'),
        ('DONE', 'D'),
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=0)
    
    # Display better information (title of entry)
    def __unicode__(self):
        return self.title